package pl.codilingus.shoppilingus.model;

public class Product {

  private String name;
  private double price;
  private String type;
  private String size;
  private double weight;
  private double tax;

  public Product(String name, double price, String type, String size, double weight, double tax) {
    this.name = name;
    this.price = price;
    this.type = type;
    this.size = size;
    this.weight = weight;
    this.tax = tax;
  }

  public Product(String name, double price, String type) {
    this(name, price, type, null, 0.0, 0.0);
  }

  public double getNettoPrice() {
    // TODO
    return 0.0;
  }

  public double getBruttoPrice() {
    // TODO
    return 0.0;
  }

}
