package pl.codilingus.shoppilingus.model;

import java.util.LinkedList;
import java.util.List;

public class Shop {

  private static int nextId = 0;

  public int id;
  public String name;
  private Location location;

  private List<String> types;
  private List<ProductDetails> products;
  private List<Service> services;
  private List<Employee> employees;

  public Shop(String name, int floor, int box, List<String> types) {
    this.id = Shop.nextId++;
    this.name = name;
    this.location = new Location(floor, box);
    this.types = types;
    this.products = new LinkedList<ProductDetails>();
    this.services = new LinkedList<Service>();
    this.employees = new LinkedList<Employee>();
  }

  public Shop(String name, int floor, int box) {
    this(name, floor, box, new LinkedList<String>());
  }

  public void hire(List<Employee> newEmployeesToHire) {
    this.employees.addAll(newEmployeesToHire);
  }

  public void hire(Employee employee) {
    this.employees.add(employee);
  }

  public void fire(Employee employee) {
    // TODO
  }

  public void addProduct(Product product, double quantity) {
    // TODO
  }

  public void addService(Service service) {
    // TODO
  }

  public boolean isEmployeeAvailable(Employee employee) {
    // TODO
    return false;
  }

  public boolean isProductAvailable(Product product) {
    // TODO
    return false;
  }

  public void buyProduct(Product product) {
    // TODO
  }

  public void buyService(Service service) {
    // TODO
  }

  public void finishService(Service service) {
    // TODO
  }

}
