package pl.codilingus.shoppilingus.model;

public class Employee {

  public String name;
  private String[] skills;
  private boolean isAvailable;

  public Employee(String name, String[] skills, boolean isAvailable) {
    this.name = name;
    this.skills = skills;
    this.isAvailable = isAvailable;
  }

  public Employee(String name) {
    this(name, new String[0], true);
  }

}
