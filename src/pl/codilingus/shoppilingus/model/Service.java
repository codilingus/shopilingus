package pl.codilingus.shoppilingus.model;

public class Service {

  private String name;
  private double price;
  private int durationInMinutes;

  public Service(String name, double price, int durationInMinutes) {
    this.name = name;
    this.price = price;
    this.durationInMinutes = durationInMinutes;
  }

}
