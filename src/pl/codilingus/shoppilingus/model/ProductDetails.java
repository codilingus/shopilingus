package pl.codilingus.shoppilingus.model;

public class ProductDetails {

  private Product product;
  private double quantity;

  public ProductDetails(Product product, double quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public ProductDetails(Product product, int quantity) {
    this(product, (double) quantity);
  }

}
