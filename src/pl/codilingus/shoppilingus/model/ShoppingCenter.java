package pl.codilingus.shoppilingus.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCenter {

  private List<Shop> shops;
  private String name;

  public ShoppingCenter(String name) {
    this.name = name;
    this.shops = new LinkedList<Shop>();
  }

  public void addShop(Shop shop) {
    this.shops.add(shop);
  }

  public Shop getShop(int shopId) {
    return shops.stream()
        .filter(shop -> shop.id == shopId)
        .findFirst()
        .orElse(null);
  }

  public void updateShop(int shopId, Shop modifiedShop) {
    deleteShop(shopId);
    modifiedShop.id = shopId;
    addShop(modifiedShop);
  }

  public void deleteShop(int shopId) {
    this.shops.remove(getShop(shopId));
  }

//  public List<Shop> findShopsByName(String name) {
//    List<Shop> result = new LinkedList<>();
//    for (Shop shop : this.shops) {
//      if (shop.name.equals(name)) {
//        result.add(shop);
//      }
//    }
//    return result;
//  }

  public List<Shop> findShopsByName(String name) {
    return shops.stream()
        .filter(shop -> shop.name.equals(name))
        .collect(Collectors.toList());
  }

}
